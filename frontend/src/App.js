import React, {useState, useEffect} from "react";
import axios from "axios";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AddEtudiant from './composant/AddEtudiant';

function App() {

  const [etudiant, setEtudiant] = useState([]);

  useEffect(() => {
    fetchEtudiant();
  }, []);

  const fetchEtudiant = async () => {
    try{
      const response = await axios.get("http://localhost:8000/etudiant");
      console.log(response.data)
      setEtudiant(response.data);
    }catch (error){
      (console.error("Error fetching etudiants ", error));
    }
  };

 return(
  <div className="container mt-5">
  <AddEtudiant/>
  <h1>Liste des étudiants</h1>
  <table className="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOM</th>
        <th>NIVEAU</th>
        <th>ACTION</th>
      </tr>
    </thead>
    <tbody>
      {etudiant.map(etudiants => (
        <tr key={etudiants.id}>
          <td>{etudiants.id}</td>
          <td>{etudiants.nom}</td>
          <td>{etudiants.niveau}</td>
          <td><button className="btn btn-danger">Supprimer</button></td>
          <td><button className="btn btn-warning">Modifier</button></td>
        </tr>
      ))}
    </tbody>
  </table>
</div>
);
}

export default App;
