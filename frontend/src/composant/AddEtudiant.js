import React, {useState} from "react";

function AddEtudiant({onAddEtudiant}){
    const [nom , setNom] = useState("");
    const [niveau , setNiveau] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        if(nom && niveau){
            onAddEtudiant({nom, niveau});
            setNiveau("");
            setNiveau("");
        }
    };

    return (
        <div className="mt-3">
          <h2>Ajouter un étudiant</h2>
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label>Nom</label>
              <input
                type="text"
                className="form-control"
                value={nom}
                onChange={(e) => setNom(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Niveau</label>
              <input
                type="text"
                className="form-control"
                value={niveau}
                onChange={(e) => setNiveau(e.target.value)}
              />
            </div>
            <button type="submit" className="btn btn-primary">Ajouter</button>
          </form>
        </div>
      );
}
export default AddEtudiant;