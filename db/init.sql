CREATE DATABASE GestioNotes;
use GestioNotes;

CREATE TABLE etudiant (
  id INT NOT NULL AUTO_INCREMENT,
  nom VARCHAR(50),
  niveau VARCHAR(50),
  PRIMARY KEY (id)
);

INSERT INTO etudiant
  (nom, niveau )
VALUES
  ('RANDRIA', 'M1'),
  ('RAKOTO', 'M2');