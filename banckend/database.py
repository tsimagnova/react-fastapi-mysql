import mysql.connector
import logging

logging.basicConfig(filename='log/app.log', level=logging.DEBUG)

db_config = {
    "host": "localhost",
    "user": "root",
    "password": "",
    "database": "gestion_de_note"
}
try:
    connection = mysql.connector.connect(**db_config)
    print("connect to the database mysql")
    logging.info("database connected")
except mysql.connector.Error as err:
    print(f"Error:{err}")
    logging.error(f"Error:{err}")
