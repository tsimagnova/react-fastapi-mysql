from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from database import db_config
from pydantic import BaseModel
import mysql.connector
import logging

app = FastAPI()

logging.basicConfig(filename='log/app.log', level=logging.DEBUG)

origins = [
    "http://localhost",
    "http://localhost:3000",  # Replace with your React frontend URL
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Etudiants(BaseModel):
    nom: str
    niveau: str

@app.post("/etudiant/")
def create_etudiants(etudiant: Etudiants):
    try:
        cursor = db_config.connection.cursor() 
        query = "INSERT INTO etudiant (nom, niveau) VALUES (%s, %s)"
        values = (etudiant.nom, etudiant.niveau)
        cursor.execute(query, values)
        db_config.connection.commit()
        logging.info("etudiant created!!")
        return{"message": "Student created"}
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail="Create student error")

@app.get("/etudiant/{id_etudiant}")
def read_etudiants(id_etudiant: int):
    cursor = db_config.connection.cursor(dictionary=True)
    query = "SELECT * FROM etudiant WHERE id = %s"
    cursor.execute(query, (id_etudiant,))
    etudiants = cursor.fetchone()
    logging.info("etudiant bien listé ")
    if etudiants is None:
        raise HTTPException(status_code=404, detail= "Student not found")
    return etudiants

@app.get("/etudiant/")
def read_etudiants():
    cursor = db_config.connection.cursor(dictionary=True)
    query = "SELECT * FROM etudiant"
    cursor.execute(query)
    etudiants = cursor.fetchall()
    logging.info("liste des étudiants bien listé ")
    if etudiants is None:
        raise HTTPException(status_code=404, detail= "Student not found")
    return etudiants

@app.delete("/etudiant/{id_etudiant}")
def delete_etudiants(id_etudiant: int):
    try:
        cursor = db_config.connection.cursor()
        query = "DELETE FROM etudiant WHERE id = %s"
        cursor.execute(query, (id_etudiant,))
        db_config.connection.commit()
        return {"message": "Student deleted"}
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail="Delete error")
    
@app.put("/etudiant/")
def update_etudiants(id_etudiant: int, etudiant: Etudiants):
    try:
        cursor = db_config.connection.cursor()
        query = "UPDATE etudiant SET nom=%s, niveau=%s WHERE id=%s"
        values = (etudiant.nom, etudiant.niveau, id_etudiant)
        cursor.execute(query,values)
        db_config.connection.commit()
        return {"message": "Student update"}
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail="Update error")
    
@app.get("/log")
def read_root():
    logging.debug("This is a debug message.")
    logging.info("This is an info message.")
    logging.warning("This is a warning message.")
    logging.error("This is an error message.")
    return {"message": "Hello, FastAPI!"}